﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Seeys.Converters
{
    class Double2CornerRadiusConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var radius = (double) value;

            return new CornerRadius(radius);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var cornerRadius = (CornerRadius) value;

            return cornerRadius.BottomLeft;
        }
    }
}
