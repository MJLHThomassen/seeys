﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Seeys.Converters
{
    class Double2ThicknessConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var radius = (double) value;

            return new Thickness(radius);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var cornerRadius = (Thickness) value;

            return cornerRadius.Left;
        }
    }
}
