﻿using System.Diagnostics;
using System.IO;
using Seeys.Extensions;
using Seeys.ViewModels;
using Seeys.Views;
using SimpleInjector;
using XmlConfigLib.Core;

namespace Seeys
{
    internal class Bootstrapper
    {
        private readonly Container _container;

        public Container Container
        {
            get { return _container; }
        }

        internal Bootstrapper()
        {
            _container = new Container();
        }

        internal Container Bootstrap()
        {
            // Register Settings
            _container.Register(SeeysSettings.LoadSettingsFile, Lifestyle.Singleton);
            _container.RegisterInitializer<SeeysSettings>(settings =>
            {
                settings.TrackedPropertyChanged += (sender, args) => settings.SaveSettingsFile();
            });

            // Register ViewModels
            _container.Register<SystemTrayViewModel>(Lifestyle.Singleton);
            _container.Register<KeysOverlayViewModel>(Lifestyle.Singleton);
            _container.Register<SettingsViewModel>(Lifestyle.Singleton);

            // Register Views
            _container.Register<SystemTray>(Lifestyle.Singleton);

            _container.Register<KeysOverlayView>(Lifestyle.Singleton);
            _container.RegisterInitializer<KeysOverlayView>(keysOverlayView =>
            {
                keysOverlayView.WithHideInsteadOfClose();
            });

            _container.Register<SettingsView>(Lifestyle.Singleton);
            _container.RegisterInitializer<SettingsView>(settingsView =>
            {
                settingsView.WithHideInsteadOfClose();
            });

            // Verify the container's configuration
            _container.Verify();

            return _container;
        }
    }
}
