﻿using System.Drawing;
using System.Windows;
using System.Windows.Interop;

namespace Seeys.Extensions
{
    public static class WindowExtensions
    {
        public static Rectangle GetScreenResolution(this Window window)
        {
            return System.Windows.Forms.Screen.FromHandle(new WindowInteropHelper(window).Handle).WorkingArea;
        }

        public static void WithHideInsteadOfClose(this Window window)
        {
            window.Closing += (sender, args) =>
            {
                // Hide instead of close the window
                args.Cancel = true;
                window.Hide();
            };
        }
    }
}
