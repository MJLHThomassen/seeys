﻿using Seeys.ViewModels;

namespace Seeys.Views
{
    /// <summary>
    /// Interaction logic for SettingsView.xaml
    /// </summary>
    public partial class SettingsView
    {
        public SettingsView(SettingsViewModel viewModel) : base(viewModel)
        {
            InitializeComponent();
            DataContext = viewModel;
        }
    }
}
