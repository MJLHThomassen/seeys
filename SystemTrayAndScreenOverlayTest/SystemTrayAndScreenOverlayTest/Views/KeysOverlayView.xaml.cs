﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Input;
using Seeys.Extensions;
using Seeys.ViewModels;

namespace Seeys.Views
{
    public partial class KeysOverlayView
    {
        private readonly KeysOverlayViewModel _viewModel;
        private bool _dragging;

        public KeysOverlayView(KeysOverlayViewModel viewModel)
        {
            InitializeComponent();
            DataContext = viewModel;

            _viewModel = viewModel;

            MouseLeftButtonDown += OnMouseLeftButtonDown;
            MouseLeftButtonUp += OnMouseLeftButtonUp;
            LocationChanged += OnLocationChanged;
            SizeChanged += OnSizeChanged;
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            SetWindowPositionFromSettingsFile();
        }

        private void OnMouseLeftButtonDown(object sender, MouseButtonEventArgs mouseButtonEventArgs)
        {
            _dragging = true;
        }

        private void OnMouseLeftButtonUp(object sender, MouseButtonEventArgs mouseButtonEventArgs)
        {
            _dragging = false;
        }

        private void OnLocationChanged(object sender, EventArgs e)
        {
            if (!_dragging)
                return;

            var resolution = this.GetScreenResolution();

            double x ;
            switch (_viewModel.SettingsFile.OverlayWindowHorizontalAlignment)
            {

                case HorizontalAlignment.Left:
                    x = Left / resolution.Width;
                    break;
                case HorizontalAlignment.Center:
                    x = (Left + ActualWidth / 2) / resolution.Width;
                    break;
                case HorizontalAlignment.Right:
                    x = (Left + Width) / resolution.Width;
                    break;
                case HorizontalAlignment.Stretch:
                    throw new NotImplementedException("Stretch OverlayWindowHorizontalAlignment is not supported yet.");
                default:
                    throw new ArgumentOutOfRangeException();
            }


            var y = (Top + ActualHeight / 2) / resolution.Height;
            _viewModel.SettingsFile.OverlayWindowRelativeLocation = new Point(x, y);
        }

        private void OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            SetWindowPositionFromSettingsFile();
        }

        private void SetWindowPositionFromSettingsFile()
        {
            var resolution = this.GetScreenResolution();

            switch (_viewModel.SettingsFile.OverlayWindowHorizontalAlignment)
            {
                case HorizontalAlignment.Left:
                    Left = _viewModel.SettingsFile.OverlayWindowRelativeLocation.X * resolution.Width;
                    break;
                case HorizontalAlignment.Center:
                    Left = _viewModel.SettingsFile.OverlayWindowRelativeLocation.X * resolution.Width - ActualWidth / 2;
                    break;
                case HorizontalAlignment.Right:
                    Left = _viewModel.SettingsFile.OverlayWindowRelativeLocation.X * resolution.Width - ActualWidth;
                    break;
                case HorizontalAlignment.Stretch:
                    throw new NotImplementedException("Stretch OverlayWindowHorizontalAlignment is not supported yet.");
                default:
                    throw new ArgumentOutOfRangeException();
            }

            Top = _viewModel.SettingsFile.OverlayWindowRelativeLocation.Y*resolution.Height - ActualHeight/2;
        }
    }
}
