﻿using System.Windows;
using Seeys.ViewModels;

namespace Seeys.Views
{
    /// <summary>
    /// Interaction logic for SystemTray.xaml
    /// </summary>
    public partial class SystemTray : Window
    {
        public SystemTray(SystemTrayViewModel systemTrayViewModel)
        {
            InitializeComponent();
            ContextMenu.DataContext = systemTrayViewModel;
        }
    }
}
