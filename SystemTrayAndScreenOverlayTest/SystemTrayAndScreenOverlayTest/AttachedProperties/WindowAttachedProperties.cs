﻿using System;
using System.ComponentModel;
using System.Windows;

namespace Seeys.AttachedProperties
{
    public static class WindowAttachedProperties
    {
        public static readonly DependencyProperty HideWindowWhenClosingProperty = DependencyProperty.RegisterAttached(
            "WindowAttachedProperties", typeof (bool), typeof (WindowAttachedProperties), new PropertyMetadata(true, HideWindowWhenClosingChangedCallback));

        public static void SetHideWindowWhenClosing(Window element, bool value)
        {
            element.SetValue(HideWindowWhenClosingProperty, value);
        }

        public static bool GetHideWindowWhenClosing(Window element)
        {
            return (bool) element.GetValue(HideWindowWhenClosingProperty);
        }

        private static void HideWindowWhenClosingChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var window = (Window) d;

            if ((bool)e.NewValue)
            {
                window.Closing += ElementOnClosing;
            }
            else
            {
                window.Closing -= ElementOnClosing;
            }
        }

        private static void ElementOnClosing(object sender, CancelEventArgs cancelEventArgs)
        {
            // In the closing event, hide the window on the UI thread then cancel the close request.
            var window = (Window)sender;

            Application.Current.Dispatcher.Invoke(window.Hide);
            cancelEventArgs.Cancel = true;
        }
    }
}
