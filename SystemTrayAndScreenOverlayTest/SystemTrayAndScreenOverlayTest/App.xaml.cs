﻿using System.IO;
using System.Windows;
using Seeys.ViewModels;
using Seeys.Views;
using SimpleInjector;
using XmlConfigLib.Core;

namespace Seeys
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        private Container _container;

        protected override void OnStartup(StartupEventArgs e)
        {
            _container = new Bootstrapper().Bootstrap();

            // Start Tray Icon
            _container.GetInstance<SystemTray>().InitializeComponent();
        }

        protected override void OnExit(ExitEventArgs e)
        {
            _container.GetInstance<SystemTray>().Close();
            base.OnExit(e);
        }
    }
}
