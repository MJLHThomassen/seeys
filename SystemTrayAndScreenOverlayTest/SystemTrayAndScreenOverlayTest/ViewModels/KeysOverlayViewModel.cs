﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using CSharpWin32HookAPI;
using Seeys.Annotations;
using Seeys.Extensions;
using XmlConfigLib.Wpf.ViewModels;
using Application = System.Windows.Application;

namespace Seeys.ViewModels
{
    public class KeysOverlayViewModel : XmlFileViewModelBase<SeeysSettings>, IDisposable
    {
        #region Fields

        private readonly Dictionary<int, Keys> _downKeys;

        #endregion

        #region Properties

        private ObservableCollection<KeyPressedInfo> _pressedKeys;

        public ObservableCollection<KeyPressedInfo> PressedKeys
        {
            get { return _pressedKeys; }
            set
            {
                if (Equals(value, _pressedKeys)) return;
                _pressedKeys = value;
            }
        }

        public bool IsAnyKeyPressed => _downKeys.Any();

        #endregion

        #region Commands

        #endregion

        #region Constructor / Destructor
        public KeysOverlayViewModel(SeeysSettings settingsFile) : base(settingsFile)
        {
            _downKeys = new Dictionary<int, Keys>();

            PressedKeys = new ObservableCollection<KeyPressedInfo>();

            Win32Hooks.LowLevelKeyboardHookEvent += Win32Hooks_LowLevelKeyboardHookEvent;
        }

        ~KeysOverlayViewModel()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // Flag: Has Dispose already been called? 
        bool _disposed = false;

        // Protected implementation of Dispose pattern. 
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                // Free any other managed objects here.
            }

            // Free any unmanaged objects here. 
            Win32Hooks.LowLevelKeyboardHookEvent -= Win32Hooks_LowLevelKeyboardHookEvent;

            _disposed = true;
        }

        #endregion

        void Win32Hooks_LowLevelKeyboardHookEvent(object sender, Win32HookEventArgs<WM, KBDLLHOOKSTRUCT> e)
        {
            if (e.WParam == WM.KEYDOWN)
            {
                if (!_downKeys.ContainsKey(e.LParam.scanCode))
                {
                    _downKeys.Add(e.LParam.scanCode, e.LParam.vkCode);
                    OnPropertyChanged(nameof(IsAnyKeyPressed));

                    Application.Current.Dispatcher.InvokeAsync(() =>
                    {
                        // Check if PressedKeys still contains the key (it was released but pressed again before the key disappeard) and remove it immediately
                        var pressedKey = PressedKeys.SingleOrDefault(x => x.ScanCode == e.LParam.scanCode);

                        if (pressedKey != null)
                        {
                            PressedKeys.Remove(pressedKey);
                        }

                        var key = new KeyPressedInfo
                        {
                            ScanCode = e.LParam.scanCode,
                            KeyString = e.LParam.vkCode.ToDisplayedString(),
                            KeyFontSize = SettingsFile.KeyFontSize,
                            KeyBorderThicknes = SettingsFile.KeyBorderThickness,
                            KeyCornerRadius = SettingsFile.KeyCornerRadius,
                        };

                        key.RemovedAction = () => PressedKeys.Remove(key);

                        PressedKeys.Add(key);
                    });
                }
            }

            if (e.WParam == WM.KEYUP)
            {
                if (_downKeys.ContainsKey(e.LParam.scanCode))
                {
                    _downKeys.Remove(e.LParam.scanCode);
                    OnPropertyChanged(nameof(IsAnyKeyPressed));
                }

                Application.Current.Dispatcher.InvokeAsync(() =>
                {
                    var key = PressedKeys.SingleOrDefault(x => x.ScanCode == e.LParam.scanCode && x.IsBeingRemoved == false);

                    if (key == null)
                        return;

                    key.IsBeingRemoved = true;
                });
            }           
        }
    }

    public class KeyPressedInfo : DependencyObject, IAnimatable
    {
        #region Properties

        public int ScanCode { get; set; }

        public string KeyString { get; set; }

        public Image KeyImage { get; set; }

        public double KeyFontSize { get; set; }

        public double KeyBorderThicknes { get; set; }

        public double KeyCornerRadius { get; set; }

        public Action RemovedAction { get; set; }
        #endregion

        #region Dependency Properties

        public static readonly DependencyProperty IsBeingRemovedProperty = DependencyProperty.Register(
            "IsBeingRemoved", typeof (bool), typeof (KeyPressedInfo), new PropertyMetadata(default(bool)));

        public bool IsBeingRemoved
        {
            get { return (bool) GetValue(IsBeingRemovedProperty); }
            set { SetValue(IsBeingRemovedProperty, value); }
        }

        public static readonly DependencyProperty RemovedProperty = DependencyProperty.Register(
            "Removed", typeof (bool), typeof (KeyPressedInfo), new PropertyMetadata(default(bool), RemovedChangedCallback));


        public bool Removed
        {
            get { return (bool) GetValue(RemovedProperty); }
            set { SetValue(RemovedProperty, value); }
        }

        private static void RemovedChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var keyPressedInfo = (KeyPressedInfo) d;

            if ((bool) e.NewValue)
            {
                keyPressedInfo.RemovedAction();
            }
        }
        #endregion

        #region IAnimatable Members (Required for animating the RemovedProperty)

        public void ApplyAnimationClock(DependencyProperty dp, AnimationClock clock)
        {
            
        }

        public void ApplyAnimationClock(DependencyProperty dp, AnimationClock clock, HandoffBehavior handoffBehavior)
        {
            
        }

        public void BeginAnimation(DependencyProperty dp, AnimationTimeline animation)
        {
            
        }

        public void BeginAnimation(DependencyProperty dp, AnimationTimeline animation, HandoffBehavior handoffBehavior)
        {
           
        }

        public object GetAnimationBaseValue(DependencyProperty dp)
        {
            return dp.DefaultMetadata.DefaultValue;
        }

        public bool HasAnimatedProperties { get; } = true;

        #endregion
    }
}