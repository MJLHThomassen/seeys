﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using Microsoft.Practices.Prism.Commands;
using Seeys.Annotations;
using Seeys.Views;

namespace Seeys.ViewModels
{
    public class SystemTrayViewModel : INotifyPropertyChanged
    {
        private readonly KeysOverlayView _keysOverlayView;
        private readonly SettingsView _settingsView;

        #region Properties

        private bool _isShowOverlayChecked = true;

        public bool IsShowOverlayChecked
        {
            get { return _isShowOverlayChecked; }
            set
            {
                if (value == _isShowOverlayChecked) return;
                _isShowOverlayChecked = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region Commands

        public DelegateCommand<bool?> ShowOverlayCommand { get; private set; }
        public DelegateCommand SettingsCommand { get; private set; }
        public DelegateCommand ExitCommand { get; private set; }

        private void ExecuteShowOverlay(bool? showOverlay)
        {
            if (showOverlay.HasValue && showOverlay.Value)
            {
                _keysOverlayView.Show();
            }
            else
            {
                _keysOverlayView.Hide();
            }
        }

        private void ExecuteSettings()
        {
            if (_isShowOverlayChecked)
                _keysOverlayView.Hide();

            _settingsView.Show();
        }

        private void ExecuteExit()
        {
            Application.Current.Shutdown();
        }
        #endregion

        #region Constructor & Initialization

        public SystemTrayViewModel(KeysOverlayView keysOverlayView,
            SettingsView settingsView)
        {
            ShowOverlayCommand = new DelegateCommand<bool?>(ExecuteShowOverlay);
            SettingsCommand = new DelegateCommand(ExecuteSettings);
            ExitCommand = new DelegateCommand(ExecuteExit);

            _keysOverlayView = keysOverlayView;
            _settingsView = settingsView;

            _settingsView.Closing += (sender, args) =>
            {
                if (_isShowOverlayChecked)
                {
                    try
                    {
                        _keysOverlayView.Show();
                    }
                    catch (InvalidOperationException)
                    {
                        // Application is closing
                    }
                }
            };

            if (_isShowOverlayChecked)
                _keysOverlayView.Show();
        }

        #endregion


        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}