﻿using System;
using System.Windows;
using XmlConfigLib.Wpf.ViewModels;

namespace Seeys.ViewModels
{
    public sealed class SettingsViewModel : XmlFileViewModelBase<SeeysSettings>
    {
        private bool _overlayWindowHorizontalAlignmentLeftChecked;
        private bool _overlayWindowHorizontalAlignmentCenterChecked;
        private bool _overlayWindowHorizontalAlignmentRightChecked;

        #region Properties

        public bool OverlayWindowHorizontalAlignmentLeftChecked
        {
            get { return _overlayWindowHorizontalAlignmentLeftChecked; }
            set
            {
                if (Equals(value, _overlayWindowHorizontalAlignmentLeftChecked)) return;
                _overlayWindowHorizontalAlignmentLeftChecked = value;
                OnPropertyChanged();

                if (value)
                    SettingsFile.OverlayWindowHorizontalAlignment = HorizontalAlignment.Left;
            }
        }

        public bool OverlayWindowHorizontalAlignmentCenterChecked
        {
            get { return _overlayWindowHorizontalAlignmentCenterChecked; }
            set
            {
                if (Equals(value, _overlayWindowHorizontalAlignmentCenterChecked)) return;
                _overlayWindowHorizontalAlignmentCenterChecked = value;
                OnPropertyChanged();

                if (value)
                    SettingsFile.OverlayWindowHorizontalAlignment = HorizontalAlignment.Center;
            }
        }

        public bool OverlayWindowHorizontalAlignmentRightChecked
        {
            get { return _overlayWindowHorizontalAlignmentRightChecked; }
            set
            {
                if (Equals(value, _overlayWindowHorizontalAlignmentRightChecked)) return;
                _overlayWindowHorizontalAlignmentRightChecked = value;
                OnPropertyChanged();

                if(value)
                    SettingsFile.OverlayWindowHorizontalAlignment = HorizontalAlignment.Right;
            }
        }

        #endregion

        #region Constructor

        public SettingsViewModel(SeeysSettings settingsFile) : base(settingsFile)
        {
            switch (settingsFile.OverlayWindowHorizontalAlignment)
            {
                case HorizontalAlignment.Left:
                    OverlayWindowHorizontalAlignmentLeftChecked = true;
                    break;
                case HorizontalAlignment.Center:
                    OverlayWindowHorizontalAlignmentCenterChecked = true;
                    break;
                case HorizontalAlignment.Right:
                    OverlayWindowHorizontalAlignmentRightChecked = true;
                    break;
                case HorizontalAlignment.Stretch:
                    // TODO
                    throw new NotImplementedException("Stretch OverlayWindowHorizontalAlignment is not supported yet.");
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        #endregion
    }
}