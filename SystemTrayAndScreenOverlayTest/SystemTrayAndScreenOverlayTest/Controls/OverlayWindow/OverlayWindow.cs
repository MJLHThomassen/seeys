﻿using System;
using System.Windows;
using System.Windows.Input;

namespace Seeys.Controls.OverlayWindow
{
    public abstract class OverlayWindow : Window
    {
        private ResizeMode _originalResizeMode;

        protected OverlayWindow()
        {
            MouseDown += OnMouseDown;
            MouseUp += OnMouseUp;
            LocationChanged += OnLocationChanged;

            WindowStartupLocation = WindowStartupLocation.Manual;
        }

        private void OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ButtonState == MouseButtonState.Pressed && e.ChangedButton == MouseButton.Left)
            {
                _originalResizeMode = ResizeMode;

                // This prevents Windows 7 and up AeroSnap
                if (ResizeMode != ResizeMode.NoResize)
                {
                    ResizeMode = ResizeMode.NoResize;
                    UpdateLayout();
                }

                DragMove();
            }
        }

        private void OnMouseUp(object sender, MouseEventArgs e)
        {
            // Restore resize mode
            ResizeMode = _originalResizeMode;
            UpdateLayout();
        }

        private void OnLocationChanged(object sender, EventArgs e)
        {
            ResizeMode = ResizeMode.NoResize;
        }
    }
}
