﻿using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Seeys.Annotations;

namespace Seeys.Controls.KeyDisplayControl
{
    class KeyDisplayControl : Control, INotifyPropertyChanged
    {

        #region Dependency Properties
        public static readonly DependencyProperty CornerRadiusProperty = DependencyProperty.Register(
            "CornerRadius", typeof (double), typeof (KeyDisplayControl), new PropertyMetadata(3.0));

        public static readonly DependencyProperty KeyFontSizeProperty = DependencyProperty.Register(
            "KeyFontSize", typeof (double), typeof (KeyDisplayControl), new PropertyMetadata(32.0));

        public static readonly DependencyProperty KeyProperty = DependencyProperty.Register(
            "Key", typeof (string), typeof (KeyDisplayControl), new PropertyMetadata(default(string)));


        public double CornerRadius
        {
            get { return (double)GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
        }

        public double KeyFontSize
        {
            get { return (double)GetValue(KeyFontSizeProperty); }
            set { SetValue(KeyFontSizeProperty, value); }
        }

        public string Key
        {
            get { return (string)GetValue(KeyProperty); }
            set { SetValue(KeyProperty, value); }
        }

        #endregion

        #region Dependency Property Changed Callbacks



        #endregion

        #region Constructor & Initialization

        public KeyDisplayControl()
        {
            DefaultStyleKey = typeof (KeyDisplayControl);
        }
        #endregion


        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
