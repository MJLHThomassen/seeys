﻿using System.Configuration;
using System.IO;
using System.ServiceModel;
using System.Windows;
using System.Windows.Media;
using System.Xml.Serialization;
using XmlConfigLib.Core;
using XmlConfigLib.Core.Attributes;

namespace Seeys
{
    public class SeeysSettings : XmlFileBase
    {
        private static string SettingsFileName = "settings.xml";

        private double _keyFontSize = 32.0;
        private double _keyBorderThickness = 2.0;
        private double _keyCornerRadius = 3.0;

        private Brush _overlayWindowBackground = new SolidColorBrush(Color.FromArgb(205, 126, 126, 126));
        private double _overlayWindowCornerRadius;
        private Point _overlayWindowRelativeLocation;
        private HorizontalAlignment _overlayWindowHorizontalAlignment;

        #region Keys Overlay Window Settings

        public double KeyFontSize
        {
            get { return _keyFontSize; }
            set
            {
                if (value.Equals(_keyFontSize)) return;
                _keyFontSize = value;
                OnPropertyChanged();
            }
        }

        public double KeyBorderThickness
        {
            get { return _keyBorderThickness; }
            set
            {
                if (value.Equals(_keyBorderThickness)) return;
                _keyBorderThickness = value;
                OnPropertyChanged();
            }
        }

        public double KeyCornerRadius
        {
            get { return _keyCornerRadius; }
            set
            {
                if (value.Equals(_keyCornerRadius)) return;
                _keyCornerRadius = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// The Overlay Window background brush.
        /// Get serialized trough the OverlayWindowBackgroundString property
        /// </summary>
        [XmlIgnore]
        public Brush OverlayWindowBackground
        {
            get { return _overlayWindowBackground; }
            set
            {
                if (value.Equals(_overlayWindowBackground)) return;
                _overlayWindowBackground = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// String for serializing/deserializing the OverlayWindowBackground nonserializable Brush type
        /// </summary>
        [DontCheckPropertyChanged]
        public string OverlayWindowBackgroundString
        {
            get
            {
                var converter = new BrushConverter();
                return (string)converter.ConvertTo(OverlayWindowBackground, typeof(string));
            }
            set
            {
                var converter = new BrushConverter();
                OverlayWindowBackground = (Brush)converter.ConvertFrom(value);
            }
        }

        public double OverlayWindowCornerRadius
        {
            get { return _overlayWindowCornerRadius; }
            set
            {
                if (value.Equals(_overlayWindowCornerRadius)) return;
                _overlayWindowCornerRadius = value;
                OnPropertyChanged();
            }
        }

        public HorizontalAlignment OverlayWindowHorizontalAlignment
        {
            get { return _overlayWindowHorizontalAlignment; }
            set
            {
                if (value.Equals(_overlayWindowHorizontalAlignment)) return;
                _overlayWindowHorizontalAlignment = value;
                OnPropertyChanged();
            }
        }

        public Point OverlayWindowRelativeLocation
        {
            get { return _overlayWindowRelativeLocation; }
            set
            {
                if (value.Equals(_overlayWindowRelativeLocation)) return;
                _overlayWindowRelativeLocation = value;
                OnPropertyChanged();
            }
        }

        #endregion

        /// <summary>
        /// Loads the settings file.
        /// </summary>
        /// <returns>The loaded settings instance.</returns>
        public static SeeysSettings LoadSettingsFile()
        {
            var settingsFile = new SeeysSettings();

            try
            {
                settingsFile = XmlHelper.LoadXmlFile<SeeysSettings>(SettingsFileName);
            }
            catch
            {
                // The settings file is corrupted or does not exist, create a new one
                settingsFile.SaveSettingsFile();
            }

            return settingsFile;
        }

        /// <summary>
        /// Saves this settings instance to the settings file.
        /// </summary>
        public void SaveSettingsFile()
        {
            XmlHelper.SaveXmlFile(this, SettingsFileName);
        }
    }
}